//
//  ViewController.swift
//  TestGIT
//
//  Created by Victoria on 11.11.21.
//

import UIKit

//test

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func showText(_ sender: Any) {
        textLabel.text = textField.text
    }
    
    @IBAction func secondVcButton(_ sender: Any) {
        let secondVc = SecondVC(nibName: String(describing: SecondVC.self), bundle: nil)
        secondVc.modalPresentationStyle = .overFullScreen
        present(secondVc, animated: true, completion: nil)
    }
}


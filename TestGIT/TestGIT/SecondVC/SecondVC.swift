//
//  SecondVC.swift
//  TestGIT
//
//  Created by Дмитрий Лапунов on 11.11.21.
//

import UIKit

class SecondVC: UIViewController {
    @IBOutlet weak var buttonLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonLabel.isHidden = true
    }
    
    @IBAction func buttonPressedAction(_ sender: Any) {
        buttonLabel.isHidden = false
        buttonLabel.text = "Ой! Меня жмякнули"
    }
}
